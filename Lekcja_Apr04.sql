CREATE DATABASE Zadania;
USE Zadania;

CREATE TABLE Pracownicy (
nazwisko TEXT,
	imie TEXT,
	wiek INT,
	departament TEXT,
	pensja INT, 
	staz INT
);

INSERT INTO Pracownicy 
Values ('Nowak', 'Michal',33, 'finanse',3057, 8),
('Kowalczyk', 'Piotr',43, 'kadry',4831, 18),
('Wojcik', 'Marcin',27, 'marketing',3312, 11),
('Wozniak', 'Pawel',31, 'zaopatrzenie',1648, 3),
('Krawczyk', 'Tomasz',55, 'produkcja',2767, 12),
('Kowalski', 'Lukasz',46, 'finanse',3374, 4),
('Dabrowski', 'Maciej',44, 'marketing',3400, 12),
('Mazur', 'Krzysztof',25, 'marketing',3030, 5),
('Pietrzak', 'Jakub',34, 'kadry',3507, 13),
('Stepien', 'Adam',45, 'finanse',4413, 19),
('Wisniewski', 'Rafal',43, 'finanse',2192, 1),
('Wlodarczyk', 'Grzegorz',40, 'kadry',2178, 5),
('Pawlak', 'Andrzej',29, 'kadry',2623, 15),
('Wieczorek', 'Wojciech',41, 'marketing',4246, 18),
('Kaminski', 'Marek',31, 'marketing',4077, 16),
('Marciniak', 'Kamil',37, 'zaopatrzenie',3156, 14),
('Zajac', 'Jan',55, 'produkcja',3687, 6),
('Michalak', 'Mateusz',32, 'produkcja',3764, 1),
('Kozlowski', 'Jacek',31, 'zaopatrzenie',4279, 15),
('Zielinski', 'Bartosz',59, 'produkcja',4105, 11),
('Adamczyk', 'Przemyslaw',46, 'marketing',2012, 16),
('Krol ', 'Robert',39, 'produkcja',2582, 2),
('Szewczyk', 'Mariusz',37, 'zaopatrzenie',3957, 7),
('Walczak', 'Artur',32, 'marketing',4051, 18),
('Lewandowski', 'Karol',29, 'marketing',3837, 14),
('Wrobel ', 'Jaroslaw',30, 'zaopatrzenie',2039, 17),
('Szymanski', 'Konrad',34, 'produkcja',4076, 18),
('Piotrowski', 'Bartlomiej',43, 'kadry',4784, 19),
('Wojciechowski', 'Radoslaw',47, 'zaopatrzenie',4082, 10),
('Bak', 'Dariusz',42, 'produkcja',2831, 6),
('Mazurek', 'Sebastian',31, 'produkcja',4439, 6),
('Cieslak', 'Daniel',27, 'produkcja',3449, 20),
('Kwiatkowski', 'Arkadiusz',40, 'kadry',3791, 13),
('Kurek', 'Jerzy',43, 'produkcja',4912, 9),
('Sobczak', 'S≥awomir',32, 'produkcja',4299, 4),
('Pawlowski', 'Aleksander',35, 'produkcja',2633, 1),
('Markiewicz', 'Szymon',51, 'finanse',3410, 1),
('Kolodziejczyk', 'Dominik',44, 'produkcja',3222, 11),
('Kaczmarek', 'Filip',21, 'produkcja',3275, 14),
('Dudek', 'Zbigniew',28, 'produkcja',3097, 11),
('Grabowski', 'Mikolaj',45, 'produkcja',3180, 2),
('Malinowski', 'Stanislaw',32, 'finanse',3949, 19),
('Mroz', 'Damian',33, 'produkcja',3818, 19),
('Nowakowski', 'Adrian',60, 'produkcja',4097, 1),
('Jankowski', 'Hubert',32, 'produkcja',1878, 1),
('Majewski', 'Cezary',33, 'finanse',4340, 17),
('Olszewski', 'Janusz',43, 'produkcja',3025, 17),
('Kacprzak', 'Patryk',21, 'produkcja',4151, 15),
('Szymczak', 'Leszek',22, 'produkcja',4001, 17),
('Lis', 'Dawid',28, 'marketing',2288, 20);

/* 1.1. SELECT ... FROM nazwa_tabeli;
	Wyszukaj zawartosc tabeli pracownicy */

/* 1.2. SELECT ... LIMIT/TOP N
	a. Wyszukaj 5 pierwszych wartoœci kolumny ‘departament’ tabeli pracownicy
	b. Wyszukaj 10 pierwszych nazwisk i imion pracowników w tabeli pracownicy
	c. Wyszukaj 20 pierwszych rekordów tabeli pracownicy */

/* 1.3. SELECT nazwy_kolumn FROM nazwa_tabeli WHERE warunek;
	a. Wyszukaj wszystkich pracowników pracuj¹cych w dziale kadr
	b. Wyszukaj wiek wszystkich pracowników pracuj¹cych w dziale produkcji
	c. Wyszukaj nazwiska osób niebêd¹cych pracownikami dzia³u finansów */

/* 1.4. SELECT ... WHERE ... AND … ;
	a. Wyszukaj wszystkich pracowników marketingu, których wiek wynosi mniej niz 30 lat
	b. Wyszukaj wszystkich osób o imieniu Arkadiusz w przedziale wiekowym 30 – 40 lat
	c. Wyszukaj nazwiska pracowników spoza dzia³u kadr i produkcji */

/* 1.5. SELECT ... WHERE ... OR … ;
	a. Wyszukaj wszystkich pracowników w przedziale wiekowym 30 – 50 lat, którzy pracuj¹ w
	dziale finansów lub produkcji
	b. Wyszukaj imiona oraz dzia³ osób, których sta¿ pracy wynosi 10 lub wiêcej lat lub ich
	miesiêczna pensja mieœci siê w przedziale 4k – 5k
	c. Podaj liczbe rekordów dla poprzedniej kwerendy */

/* 1.6. SELECT ... WHERE ... XOR … ;
	a. Wyszukaj imiona oraz dzial osób, których albo sta¿ pracy wynosi 10 lub wiêcej lat albo
	ich miesieczna pensja miesci sie w przedziale 4k – 5k
	b. Podaj liczbê rekordów dla poprzedniej kwerendy */


/* 1.7. Aliasowanie SELECT nazwa AS ‘inna_nazwa’
	a. Pobierz z tabeli pracownicy dane o imionach, nazwiskach i dziale,
	   tak aby kolumna pensja miala nazwe wynagrodzenie
	b. Wyszukaj liczbê pracowników w dziale finansów poprzez odwolanie siê
	   do kolumny ‘departament’ za pomoca aliasu dzial */

/* 1.8. LIKE
	a. Wyszukaj pracowników o imionach zaczynajacych sie na „Mi”
	b. Wyszukaj nazwisk zaczynajacych siê na litere „A” */

/* 1.9. DISTINCT
	a. Wyswietl nazwy wszystkich dzialów dostepnych w tabeli ‘pracownicy’
	b. Podaj liczbe imion bez duplikatów */

/* 1.10. ORDER BY
	a. Wyœwietl pracowników posortowanych wzgledem kolumny ‘nazwisko’
	   rosnaco w porzadku alfabetycznym
	b. Wyswietl pracowników posortowanych wzgledem kolumny ‘dzial’
	   rosnaco w porzadku alfabetycznym a malejaco wedlug ich wynagrodzenia */

/* 1.11. GROUP BY
	a. Wyswietl przecietne wynagrodzenie w kazdym dziale, uzyj aliasu
	   przecietne_wynagrodzenie dla sredniej pensji
	b. Dla kazdego dzialu wyszukaj najdluzszy i najkrótszy staz pracy,
	   uzyj aliasu max_staz i min_staz
	c. Wyswietl liczbe pracowników pogrupowanych wedlug departamentu oraz stazu pracy
	d. Dla kazdego dzialu wyswietl liczbe pracowników, ktorych wynagrodzenie nie przekracza 3000
	   a staz pracy 10 lat, dla liczby pracowników uzyj aliasu ‘count’,
	   nastepnie posortuj wyniki malejaco wedlug liczby pracowników */


/* 1.12. HAVING
	a. Wyswietl liczbe pracowników oraz nazwy departamentów, w których liczba osób
	   ze stazem równym lub dluzszym 10 lat oraz pensja w przedziale 4000 a 5000 jest wieksza od 2
	b. Do utworzonej kwerendy dodaj: sredni wiek, przecietne wynagrodzenie oraz przecietny staz
	c. Do kwerendy z pkt b dodaj alias sredni_wiek dla przecietnego wieku i posortuj wyniki
	   rosnaco wedlug tej kolumny */

/* 1.13. CASE
	a. Wyswietl liste pracowników wraz z kolumna ‘ranga’, w której przypisane zostana nastepujace wartosci wedlug ponizszych warunków:
	WHEN staz < 3 THEN 'junior'
	WHEN staz BETWEEN 10 AND 15 THEN 'senior'
	WHEN staz > 15 THEN 'expert'
	ELSE 'mid'
	b. Posortuj wyszukane wyzej rekordy malejaco wedlug kolumny ‘ranga’ */
