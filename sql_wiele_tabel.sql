/* 1.a. Napisz kwerendę, która zwróci wszystkich klientów z zamówieniami
		zrealizowanymi w dniu 2008-01-08 */



/* 1.b. Napisz kwerendę, która zwróci listę agentów (użyj aliasu salesman)
		oraz klientów wraz z ich obszarem działalności,
		którzy należą do tego samego obszaru */





/* INNER JOIN a. Wyszukaj listę klientów, którzy podjęli współpracę z agentami
				 których prowizja jest powyżej 12% */



/* INNER JOIN b. Wyszukaj szczegóły dot. Zamówień: nr zamówienia, datę, kwotę, klienta oraz agenta,
				 który pracuje dla tego klienta oraz jego prowizję od zamówienia*/


/* LEFT JOIN a. Wyszukaj listę - posortowaną rosnąco wg kodu klienta (jego id) -
				klientów pracujących zarówno indywidualnie, jak również za pośrednictwem pośredników */



/* RIGHT JOIN a. Wyszukaj listę pośredników pracujących dla jednego bądź więcej klientów lub takich,
którzy jeszcze nie podjęli współpracy z żadnym klientem,
posortuj listę rosnąco wg kolumny kodu pośrednika (agenta) */



/* CROSS JOIN a. Wyszukaj iloczyn kartezjański pośredników oraz klientów, w taki sposób,
że każdy pośrednik będzie widoczny dla wszystkich klientów i vice versa. */



/* UNION a. Wyszukaj wszystkich pośredników (przypisz im wartość ‘Salesman’)
oraz klientów (wartość ‘Customer’) zlokalizowanych w Londynie */


/* UNION b. Napiszmy raport pokazujący który pośrednik przyjął
			największe i najmniejsze zamówienie na każdy dzień */



-- 1. Znaleźć największe zamówienie w każdym dniu

-- 2. Dla powyższej tabeli znajdźmy agenta który obsłużył zamówienie o takiej kwocie


