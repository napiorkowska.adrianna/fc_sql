/* 1.a. Napisz kwerendę, która zwróci wszystkich klientów z zamówieniami
		zrealizowanymi w dniu 2008-01-08 */

select customer.cust_code, cust_name, ord_date
from customer
join orders on customer.cust_code = orders.cust_code
where orders.ord_date = '2008-01-08'
;
-- Error Code: 1052. Column 'cust_code' in field list is ambiguous

select *
from orders
where ord_date = '2008-01-08';


select c.cust_code, cust_name, ord_date
from customer as c
join orders as o on c.cust_code = o.cust_code
where o.ord_date = '2008-01-08'
;

select c.cust_code, cust_name, ord_date
from customer c
	join orders o on c.cust_code = o.cust_code
where o.ord_date = '2008-01-08'
;

select c.cust_code, cust_name, ord_date
from customer c
	inner join orders o on c.cust_code = o.cust_code
where o.ord_date = '2008-01-08'
;

select c.cust_code, cust_name, ord_date
from customer c
	left join orders o on c.cust_code = o.cust_code
where o.ord_date = '2008-01-08'
;

select c.cust_code, cust_name, ord_date
from customer c
	left join orders o on c.cust_code = o.cust_code and o.ord_date = '2008-01-08'
;

select c.cust_code, cust_name, ord_date
from customer c
	inner join orders o on c.cust_code = o.cust_code and o.ord_date = '2008-01-08'
;


-- Napisz kwerendę, która zwróci wszystkich klientów z zamówieniami zrealizowanymi w dniu 2008-01-08 */
select cust_code, cust_name from customer;

select ord_num, ord_date, cust_code from orders where ord_date = '2008-01-08';

select *
from customer 
inner join orders on customer.cust_code = orders.cust_code
where ord_date = '2008-01-08';


/* 1.b. Napisz kwerendę, która zwróci listę agentów (użyj aliasu salesman)
		oraz klientów wraz z ich obszarem działalności,
		którzy należą do tego samego obszaru */

Select 
agents.agent_code, 
agents.agent_name, 
agents.working_area, 
customer.agent_code,
customer.cust_code, 
customer.cust_name, 
customer.working_area
from agents
join customer 
	on agents.agent_code = customer.agent_code
		and agents.working_area != customer.working_area;


/* INNER JOIN a. Wyszukaj listę klientów, którzy podjęli współpracę z agentami
				 których prowizja jest powyżej 12% */

select * from agents;

select *
from customer
inner join agents 
	on agents.agent_code = customer.agent_code
		and commission > 0.12;


/* INNER JOIN b. Wyszukaj szczegóły dot. Zamówień: nr zamówienia, datę, kwotę, klienta oraz agenta,
				 który pracuje dla tego klienta oraz jego prowizję od zamówienia*/
                 
                 


/* LEFT JOIN a. Wyszukaj listę - posortowaną rosnąco wg kodu klienta (jego id) -
				klientów pracujących zarówno indywidualnie, jak również za pośrednictwem pośredników */



/* RIGHT JOIN a. Wyszukaj listę pośredników pracujących dla jednego bądź więcej klientów lub takich,
którzy jeszcze nie podjęli współpracy z żadnym klientem,
posortuj listę rosnąco wg kolumny kodu pośrednika (agenta) */



/* CROSS JOIN a. Wyszukaj iloczyn kartezjański pośredników oraz klientów, w taki sposób,
że każdy pośrednik będzie widoczny dla wszystkich klientów i vice versa. */



/* UNION a. Wyszukaj wszystkich pośredników (przypisz im wartość ‘Salesman’)
oraz klientów (wartość ‘Customer’) zlokalizowanych w Londynie */


/* UNION b. Napiszmy raport pokazujący który pośrednik przyjął
			największe i najmniejsze zamówienie na każdy dzień */

-- 1. Znaleźć największe zamówienie w każdym dniu

-- 2. Dla powyższej tabeli znajdźmy agenta który obsłużył zamówienie o takiej kwocie



With london_customers as (
select 
cust_code as id, 
cust_name as name,
working_area, 
'customer' as record_type
from customer where working_area = 'London'
),
london_agents as (
select 
agent_code as id, 
agent_name as name,
working_area, 
'agents' as record_type
from agents where working_area = 'London'
)
select * from london_customers
union
select * from london_agents
;